<?php

namespace App\Http\Controllers;
use Illuminate\Http\Request;
use Response;
use App\Http\Controllers\Controller;


class api_basic extends Controller
{
        public function index(){
            echo "API test";
        }
        public function licz(Request $request){
          try {
            $longitude1 = $request->point1['longitude'];
            $longitude2 = $request->point2['longitude'];
            $latitude1 = $request->point1['latitude'];
            $latitude2 = $request->point2['latitude'];
      
            $diff_longitude = abs($longitude1 - $longitude2);
            $diff_latitude = abs($latitude1 - $latitude2);
            $score =
              (sqrt(
                pow($diff_latitude, 2) +
                  pow(cos(($latitude1 * 3.14) / 180) * $diff_longitude, 2)
              ) *
                40075.704) /
              360;
              
            $result=Response::json(array('status' => 'ok', 'score' => $score));
          } catch (Exception $e) {
            $result=Response::json(array('status' => 'error', 'msg' => $e));
          }
          return $result;
        }
}