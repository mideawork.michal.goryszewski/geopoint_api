<!DOCTYPE html>
<html lang="en"><head><meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width,initial-scale=1">
    <link rel="icon" href="/favicon.ico"><title>geopoint</title>
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/5.15.3/css/all.min.css" integrity="sha512-iBBXm8fW90+nuLcSKlbmrPcLa0OT92xO1BIsZ+ywDWZCvqsWgccV3gFoRBv0z+8dLJgyAHIhR35VZc2oM/gI1w==" crossorigin="anonymous">
    <link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Roboto:100,300,400,500,700,900">
    <link href="/css/app.e35730e9.css" rel="preload" as="style">
    <link href="/css/chunk-vendors.6630b637.css" rel="preload" as="style">
    <link href="/js/app.6fce4394.js" rel="preload" as="script">
    <link href="/js/chunk-vendors.25eabf9e.js" rel="preload" as="script">
    <link href="/css/chunk-vendors.6630b637.css" rel="stylesheet">
    <link href="/css/app.e35730e9.css" rel="stylesheet"></head>

    <body><noscript><strong>We're sorry but geopoint doesn't work properly without JavaScript enabled. Please enable it to continue.</strong></noscript>
    <div id="app"></div>
    <script src="/js/chunk-vendors.25eabf9e.js"></script>
    <script src="/js/app.6fce4394.js"></script></body>
</html>